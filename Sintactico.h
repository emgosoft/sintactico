#ifndef SINTACTICO_H_INCLUDED
#define SINTACTICO_H_INCLUDED
#include <stack>
#include <fstream>
#include <vector>
#include "Lexico.h"

#define ES_DECLARACION_VAR 6
#define ENT_DECLARACION_VAR 4
#define ES_DEFINICION_VAR 6
#define ENT_DEFINICION_VAR 5
#define ES_IF 6
#define ENT_IF 5

enum {TIPO_DATO_1=0,NOMBRE_IDENTIFICADOR_1,COMA_1,PUNTO_Y_COMA_1} e_declaracionVariable;
enum {TIPO_DATO_2=0,NOMBRE_IDENTIFICADOR_2,IGUAL_2,VALOR_2,PUNTO_Y_COMA_2} e_definicionVariable;
enum {IF_3=0,PARENTESIS_A_3,CONDICION_3,PARENTESIS_B_3,LLAVE_3} e_if;
enum {Q0=0,Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8} estados;

struct Linea {
    int num;
    string contenido;
    int longitud;
    int tipo;
};

class Sintactico {
    Lexico* lexico;
    int declaracionVariable[ES_DECLARACION_VAR][ENT_DECLARACION_VAR];
    int definicionVariable[ES_DEFINICION_VAR][ENT_DEFINICION_VAR];
    int declaracionIf[ES_IF][ENT_IF];
    vector<Linea> lineas;

    string archivo;
    string codigo;
    int numLinea;
    bool hayUnIf = false;
public:
    Sintactico(Lexico* lexico) {
        numLinea = 0;
        this->lexico = lexico;
        llenarAutomatas();
    }
    ~Sintactico() {
        delete this->lexico;
    }
    Lexico* dameLexico() {
        return this->lexico;
    }
    string leerArchivo(string nombreArchivo) {
        string cadena;
        ifstream leer;
        char temp;
        cadena = "";
        leer.open(nombreArchivo);
        if(leer.good()) {
            while(!leer.eof()) {
                leer.read((char *)&temp, 1);
                if(leer.eof()) break;
                cadena += temp;
            }
            leer.close();
        } else {
            cout << "No se pudo abrir el archivo" << endl;
        }
        return cadena;
    }
    bool validarPilaLlaves(string codigo) {
        int contador = 0;
        for(int i = 0; i < (int)codigo.length(); i++) {
            if(codigo[i] == '{' ) {
                contador++;
            } else if(codigo[i] == '}') {
                if(contador == 0) {
                    return false;
                } else {
                    contador--;
                }
            }
        }
        return contador == 0;
    }
    bool analizarArchivo(string nombreAchivo) {
        lineas.clear();
        codigo = leerArchivo(nombreAchivo);
        if(validarPilaLlaves(codigo)) {
            archivo = nombreAchivo;
            leerLineas(codigo);
            numLinea = 0;
            return analizarLineas(numLinea,lineas.size());
        } else {
            cout << endl << "\tNo hay equivalencia de llaves {}" << endl;
            return false;
        }
    }
    void leerLineas(string codigo) {
        int tamanoCodigo = codigo.length();
        string linea;
        Linea l;
        for(int i=0; i<=tamanoCodigo; i++) {
            if((int)codigo[i] != 10) {
                linea+=codigo[i];
            } else {
                l.contenido = linea;
                l.longitud = linea.length();
                l.num = i+1;
                lineas.push_back(l);
                linea = "";
            }
        }
    }
    bool analizarLineas(int lineaInicial, int lineaFinal) {
        //int cantidadLineas = lineas.size();
        for(numLinea=lineaInicial; numLinea<lineaFinal; numLinea++) {
            if(!analizarLinea(lineas[numLinea].contenido)) {
                cout << endl << "\tError en l�nea " << numLinea+1 << " '" << lineas[numLinea].contenido << "'" << endl;
                return false;
            }
        }
        return true;
    }
    bool analizarLinea(string linea) {
        cout << endl << "\t" <<  numLinea+1 << " " << linea << endl;
        /*cout << "\t";
        system("pause");*/
        if(linea.length() > 0) {
            if(!analizarDeclaracionVariable(linea)) {
                if(!analizarDefinicionVariable(linea)) {
                    if(!analizarDeclaracionIf(linea)) {
                        if(!haySoloLlaves(linea)) {
                             return false;
                        }
                    }
                }
            }
        }
        return true;
    }
    bool haySoloLlaves(string linea) {
        int tamano = linea.length();
        //cout << "linea: *" << linea << "*" <<  endl;
        for(int i=0; i<tamano; i++) {
            if((linea[i] != ' ') && (linea[i] != '}')) {
                return false;
            }
        }
        return true;
    }
    void llenarAutomatas() {
        //Declaraci�n de variable
        llenarDeclaracionVariable();

        //Definci�n de variable
        llenarDefinicionVariable();

        //Declaraci�n de If
        llenarDeclaracionIf();
    }
    void llenarDeclaracionIf() {
        //Forma : if ( condicion ) {
        declaracionIf[Q0][IF_3]=Q1;
        declaracionIf[Q0][PARENTESIS_A_3]=Q6;
        declaracionIf[Q0][CONDICION_3]=Q6;
        declaracionIf[Q0][PARENTESIS_B_3]=Q6;
        declaracionIf[Q0][LLAVE_3]=Q6;

        declaracionIf[Q1][IF_3]=Q6;
        declaracionIf[Q1][PARENTESIS_A_3]=Q2;
        declaracionIf[Q1][CONDICION_3]=Q6;
        declaracionIf[Q1][PARENTESIS_B_3]=Q6;
        declaracionIf[Q1][LLAVE_3]=Q6;

        declaracionIf[Q2][IF_3]=Q6;
        declaracionIf[Q2][PARENTESIS_A_3]=Q6;
        declaracionIf[Q2][CONDICION_3]=Q3;
        declaracionIf[Q2][PARENTESIS_B_3]=Q6;
        declaracionIf[Q2][LLAVE_3]=Q6;

        declaracionIf[Q3][IF_3]=Q6;
        declaracionIf[Q3][PARENTESIS_A_3]=Q6;
        declaracionIf[Q3][CONDICION_3]=Q6;
        declaracionIf[Q3][PARENTESIS_B_3]=Q4;
        declaracionIf[Q3][LLAVE_3]=Q6;

        declaracionIf[Q4][IF_3]=Q6;
        declaracionIf[Q4][PARENTESIS_A_3]=Q6;
        declaracionIf[Q4][CONDICION_3]=Q6;
        declaracionIf[Q4][PARENTESIS_B_3]=Q6;
        declaracionIf[Q4][LLAVE_3]=Q5;

        declaracionIf[Q5][IF_3]=Q6;
        declaracionIf[Q5][PARENTESIS_A_3]=Q6;
        declaracionIf[Q5][CONDICION_3]=Q6;
        declaracionIf[Q5][PARENTESIS_B_3]=Q6;
        declaracionIf[Q5][LLAVE_3]=Q6;
    }
    void llenarDefinicionVariable() {
        //Forma : tipoDato identificador = valor;
        //Forma : identificador = valor;
        definicionVariable[Q0][TIPO_DATO_2]=Q3;
        definicionVariable[Q0][NOMBRE_IDENTIFICADOR_2]=Q1;
        definicionVariable[Q0][IGUAL_2]=Q6;
        definicionVariable[Q0][VALOR_2]=Q6;
        definicionVariable[Q0][PUNTO_Y_COMA_2]=Q6;

        definicionVariable[Q1][TIPO_DATO_2]=Q6;
        definicionVariable[Q1][NOMBRE_IDENTIFICADOR_2]=Q6;
        definicionVariable[Q1][IGUAL_2]=Q2;
        definicionVariable[Q1][VALOR_2]=Q6;
        definicionVariable[Q1][PUNTO_Y_COMA_2]=Q6;

        definicionVariable[Q2][TIPO_DATO_2]=Q6;
        definicionVariable[Q2][NOMBRE_IDENTIFICADOR_2]=Q6;
        definicionVariable[Q2][IGUAL_2]=Q6;
        definicionVariable[Q2][VALOR_2]=Q4;
        definicionVariable[Q2][PUNTO_Y_COMA_2]=Q6;

        definicionVariable[Q3][TIPO_DATO_2]=Q6;
        definicionVariable[Q3][NOMBRE_IDENTIFICADOR_2]=Q1;
        definicionVariable[Q3][IGUAL_2]=Q6;
        definicionVariable[Q3][VALOR_2]=Q6;
        definicionVariable[Q3][PUNTO_Y_COMA_2]=Q6;

        definicionVariable[Q4][TIPO_DATO_2]=Q6;
        definicionVariable[Q4][NOMBRE_IDENTIFICADOR_2]=Q6;
        definicionVariable[Q4][IGUAL_2]=Q6;
        definicionVariable[Q4][VALOR_2]=Q6;
        definicionVariable[Q4][PUNTO_Y_COMA_2]=Q5;

        definicionVariable[Q5][TIPO_DATO_2]=Q6;
        definicionVariable[Q5][NOMBRE_IDENTIFICADOR_2]=Q6;
        definicionVariable[Q5][IGUAL_2]=Q6;
        definicionVariable[Q5][VALOR_2]=Q6;
        definicionVariable[Q5][PUNTO_Y_COMA_2]=Q6;
    }
    void llenarDeclaracionVariable() {
        //Forma : tipoDato identificador1,identificador2,identificadorN;
        declaracionVariable[Q0][TIPO_DATO_1]=Q1;
        declaracionVariable[Q0][NOMBRE_IDENTIFICADOR_1]=Q5;
        declaracionVariable[Q0][COMA_1]=Q5;
        declaracionVariable[Q0][PUNTO_Y_COMA_1]=Q5;

        declaracionVariable[Q1][TIPO_DATO_1]=Q5;
        declaracionVariable[Q1][NOMBRE_IDENTIFICADOR_1]=Q2;
        declaracionVariable[Q1][COMA_1]=Q5;
        declaracionVariable[Q1][PUNTO_Y_COMA_1]=Q5;

        declaracionVariable[Q2][TIPO_DATO_1]=Q5;
        declaracionVariable[Q2][NOMBRE_IDENTIFICADOR_1]=Q5;
        declaracionVariable[Q2][COMA_1]=Q4;
        declaracionVariable[Q2][PUNTO_Y_COMA_1]=Q3;

        declaracionVariable[Q3][TIPO_DATO_1]=Q5;
        declaracionVariable[Q3][NOMBRE_IDENTIFICADOR_1]=Q5;
        declaracionVariable[Q3][COMA_1]=Q5;
        declaracionVariable[Q3][PUNTO_Y_COMA_1]=Q5;

        declaracionVariable[Q4][TIPO_DATO_1]=Q5;
        declaracionVariable[Q4][NOMBRE_IDENTIFICADOR_1]=Q2;
        declaracionVariable[Q4][COMA_1]=Q5;
        declaracionVariable[Q4][PUNTO_Y_COMA_1]=Q5;
    }
    int estadoSigDeclaracionVariable(int estadoActual, int simbolo) {
        return declaracionVariable[estadoActual][simbolo];
    }
    int estadoSigDefinicionVariable(int estadoActual, int simbolo) {
        return definicionVariable[estadoActual][simbolo];
    }
    bool verificarBalanceoLlaves(string nombreArchivo) {
        ifstream archivo(nombreArchivo);
        char linea[100];
        string cadena;
        if(archivo.is_open()) {
            while(!archivo.eof()) {
                archivo.getline(linea,100);
                cadena+=dameOperadoresLinea(linea,'{','}');
            }
            archivo.close();
            return estaBalanceadaLaPila(cadena,'(');
        } else {
            return false;
        }
    }
    string dameOperadoresLinea(string linea, char caracter_1, char caracter_2) {
        int tamanoLinea = linea.length();
        char simbolo;
        string cadena = "";
        for(int i=0; i<tamanoLinea; i++) {
            simbolo=linea[i];
            if((simbolo == caracter_1) || (simbolo == caracter_2)) {
                cadena+=simbolo;
            }
        }
        if(cadena.length() > 0) {
            return cadena;
        }
        return "";
    }
    bool estaBalanceadaLaPila(string cadena, char caracterApertura) {
        stack<char> pila;
        int tamanoLinea = cadena.length();
        char simbolo;
        bool balanceada = true;
        for(int i=0; i<tamanoLinea; i++) {
            simbolo = cadena[i];
            if(simbolo == caracterApertura) {
                pila.push(simbolo);
            } else {
                if(pila.empty()) {
                    balanceada = false;
                    break;
                } else {
                    pila.pop();
                }
            }
        }
        if((balanceada) && (pila.empty())) {
            return true;
        }
        return false;
    }
    int buscarPrimerCaracter(string linea) {
        int tamanoLinea = linea.length();
        for(int i=0; i<tamanoLinea; i++) {
            if(isalpha(linea[i])) {
                return i;
            }
        }
        return -1;
    }
    bool analizarDeclaracionIf(string linea) {
        int tamanoLinea = linea.length();
        int entrada, i, indice, estado;
        string subCadena, condicion;
        int lineaFinalBloque;

        i=indice=estado=0;
        i=buscarPrimerCaracter(linea);
        indice = obtenerIndiceSubcadena(linea,i,'(');
        subCadena = obtenerSubCadena(linea,i,indice);
        if(subCadena != "if") {
            indice = obtenerIndiceSubcadena(linea,i,' ');
            subCadena = obtenerSubCadena(linea,i,indice);
        }
        while(i < tamanoLinea) {
            //cout << endl << "*" << subCadena << "*";
            if(subCadena == "if") {
                entrada = IF_3;
                i+=subCadena.length();
                indice = obtenerIndiceSubcadena(linea,i,'(');
                subCadena = linea[indice];
            } else if (subCadena == "(") {
                entrada = PARENTESIS_A_3;
                i+=subCadena.length()-1;
                indice = obtenerIndiceSubcadena(linea,i,'{');
                subCadena = obtenerSubCadena(linea,i,indice);
                if(!estaBalanceadaLaPila(dameOperadoresLinea(subCadena,'(',')'),'(')) {
                    return false;
                }
                condicion = subCadena.substr(1,subCadena.length()-3);
                estado = estadoSigDeclaracionVariable(estado,entrada);
                entrada = CONDICION_3;
                i+=condicion.length()+1;
                subCadena = linea[i];
            } else if (subCadena == ")") {
                entrada = PARENTESIS_B_3;
                i++;
                indice = obtenerIndiceSubcadena(linea,i,'{');
                subCadena = linea[indice];
            } else {
                break;
            }
            estado = estadoSigDeclaracionVariable(estado,entrada);
        }
        if(estado == Q5) {
            hayUnIf = true;
            lineaFinalBloque = obtenerIndiceBloqueInstrucciones(numLinea);
            if(lineaFinalBloque == -1) {
                return false;
            }
            numLinea++;
            for(; numLinea<lineaFinalBloque; numLinea++) {
                if(!analizarLinea(lineas[numLinea].contenido)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
    bool analizarLineasDeBloque(int lineainicial, int lineaFinal) {
        bool valido = analizarLineas(lineainicial,lineaFinal);
        numLinea+=2;
        return valido;
    }
    int obtenerIndiceBloqueInstrucciones(int linea) {
        int contLineas = lineas.size();
        int contLlaves = 1;
        for(int i=linea+1; i<contLineas; i++) {
            contLlaves = encontrarLlaves(lineas[i].contenido,contLlaves);
            cout << endl << "\tSub: " << lineas[i].contenido <<  "Llaves: " << contLlaves << endl;
            /*cout << "\t";
            system("pause");*/
            if(contLlaves == 0) {
                return i;
            } else if(contLlaves < 1) {
                return -1;
            }
        }
        return 0;
    }
    int encontrarLlaves(string linea, int contLlaves) {
        int tamanoLinea = linea.length();
        int cont = contLlaves;
        for(int i=0; i<tamanoLinea; i++) {
            if(linea[i] == '{') {
                cont++;
            } else if (linea[i] == '}') {
                cont--;
            }
        }
        return cont;
    }
    bool analizarDeclaracionVariable(string linea) {
        int tamanoLinea = linea.length();
        int entrada, i, indice, estado;
        string subCadena;

        i=indice=estado=0;
        i=buscarPrimerCaracter(linea);
        indice = obtenerIndiceSubcadena(linea,i,' ');
        subCadena = obtenerSubCadena(linea,i,indice);
        while(i < tamanoLinea) {
            //cout << endl << "-" << subCadena << "-" << endl;
            if(lexico->esTipoDeDato(subCadena)) {
                entrada = TIPO_DATO_1;
                i+=subCadena.length()+1;
                indice = obtenerIndiceSubcadena(linea,i,',');
                if(indice == 0) {
                    indice = obtenerIndiceSubcadena(linea,i,';');
                }
                subCadena = obtenerSubCadena(linea,i,indice);
            } else if (lexico->esIdentificadorVariable(subCadena)) {
                entrada = NOMBRE_IDENTIFICADOR_1;
                i+=subCadena.length();
                indice++;
                subCadena = obtenerSubCadena(linea,i,indice);
            } else if (subCadena == ";") {
                entrada = PUNTO_Y_COMA_1;
                i++;
            } else if (subCadena == ",") {
                entrada = COMA_1;
                i++;
                indice = obtenerIndiceSubcadena(linea,i,',');
                if(indice == 0) {
                    indice = obtenerIndiceSubcadena(linea,i,';');
                }
                subCadena = obtenerSubCadena(linea,i,indice);
            } else {
                break;
            }
            estado = estadoSigDeclaracionVariable(estado,entrada);
        }
        //Estado de aceptaci�n?
        if(estado == Q3) {
            return true;
        }
        return false;
    }
    bool analizarDefinicionVariable(string linea) {
        int tamanoLinea = linea.length();
        int entrada, i, indice, estado;
        string subCadena;
        string valor;

        i=indice=estado=0;
        i=buscarPrimerCaracter(linea);
        indice = obtenerIndiceSubcadena(linea,i,' ');
        subCadena = obtenerSubCadena(linea,i,indice);
        while(i < tamanoLinea) {
            //cout << endl << "+" << subCadena << "+" << endl;
            if(lexico->esTipoDeDato(subCadena)) {
                entrada = TIPO_DATO_2;
                i+=subCadena.length()+1;
                indice = obtenerIndiceSubcadena(linea,i,'=');
                indice--;
                subCadena = obtenerSubCadena(linea,i,indice);
            } else if (lexico->esIdentificadorVariable(subCadena)) {
                entrada = NOMBRE_IDENTIFICADOR_2;
                i+=subCadena.length();
                i++;
                indice = i;
                subCadena = obtenerSubCadena(linea,i,indice);
            } else if (subCadena == "=") {
                i+=2;
                indice = obtenerIndiceSubcadena(linea,i,';');
                valor = obtenerSubCadena(linea,i,indice);
                i = indice;
                subCadena = obtenerSubCadena(linea,i,indice+1);
                entrada = IGUAL_2;
                estado = estadoSigDefinicionVariable(estado,entrada);
                entrada = VALOR_2;
            } else if (subCadena == ";") {
                entrada = PUNTO_Y_COMA_2;
                i++;
            } else {
                break;
            }
            estado = estadoSigDefinicionVariable(estado,entrada);
        }
        //Estado de aceptaci�n?
        if(estado == Q5) {
            return true;
        }
        return false;
    }
    string obtenerSubCadena(string cadena, int pos1, int pos2) {
        stringstream cadenaCorta;
        int caracteresALaDererecha = pos2 - pos1;
        switch(caracteresALaDererecha) {
        case 0:
            cadenaCorta << cadena[pos1];
            return cadenaCorta.str();
        case 1:
            cadenaCorta << cadena[pos1];
            return cadenaCorta.str();
        default:
            return cadena.substr(pos1,caracteresALaDererecha);
        }
    }
    int obtenerIndiceSubcadena(string linea, int indice, char caracter) {
        int tamanoLinea = linea.length();
        if(tamanoLinea) {
            for(int i=indice; i<tamanoLinea; i++) {
                if(linea[i] == caracter) {
                    return i;
                }
            }
            return 0;
        } else {
            return -1;
        }
    }
};

#endif
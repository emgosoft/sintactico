#include <iostream>
#include <cstdlib>
#include "Sintactico.h"

#define LEXICO '1'
#define SINTACTICO '2'
#define SALIR '3'

using namespace std;

string escanearCadena();
void analizarLexicoCadena();
void analizarSintacticoCadena();
char seleccionarOpcion();
string escanearNombreArchivo();

Sintactico* sintactico = new Sintactico(new Lexico);

int main() {
    bool continuar = true;
    do {
        switch(seleccionarOpcion()) {
        case LEXICO:
            analizarLexicoCadena();
            break;
        case SINTACTICO:
            analizarSintacticoCadena();
            break;
        case SALIR:
            continuar = false;
            break;
        default:
            ;
        }
    } while(continuar);
    delete sintactico;
    return 0;
}

char seleccionarOpcion() {
    char opc;
    setlocale(LC_CTYPE,"Spanish");
    system("CLS");
    cout << endl << "\t\tCompilador" << endl;
    cout << endl << "\tIngrese opci�n: ";
    cout << endl << "\t1. Analizador L�xico";
    cout << endl << "\t2. Analizador Sint�ctico";
    cout << endl << "\t3. Salir" << endl;
    cout << endl << "\tIngrese opci�n: ";
    cin >> opc;
    return opc;
}

void analizarLexicoCadena() {
    Lexico* lexico = sintactico->dameLexico();
    char opc;
    bool continuarAnalizando = true;
    cin.get();
    while(continuarAnalizando) {
        lexico->analizarCadena(escanearCadena());
        cout << endl << endl << "\t�Desea continuar analizando cadenas (S/N)? ";
        cin >> opc;
        if((opc == 's') || (opc == 'S')) {
            continuarAnalizando = true;
        } else {
            continuarAnalizando = false;
        }
        cin.get();
    }
}

void analizarSintacticoCadena() {
    char opc;
    bool continuarAnalizando = true;
    cin.get();
    while(continuarAnalizando) {
        if(sintactico->analizarArchivo(escanearNombreArchivo())) {
            cout << endl << "\tC�digo escrito v�lido" << endl;
        } else {
            cout << "\tError de sint�xis" << endl;
        }
        cout << endl << endl << "\t�Desea continuar analizando cadenas (S/N)? ";
        cin >> opc;
        if((opc == 's') || (opc == 'S')) {
            continuarAnalizando = true;
        } else {
            continuarAnalizando = false;
        }
        cin.get();
    }
}